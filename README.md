![@inspire11/{{$NAME}}][logo]

![Version](https://img.shields.io/npm/v/@inspire11/{{$NAME}})
![Downloads](https://img.shields.io/npm/dt/@inspire11/{{$NAME}})
![Build](https://img.shields.io/bitbucket/pipelines/inspire11/{{$NAME}}/master)
![License](https://img.shields.io/npm/l/@inspire11/{{$NAME}})

# {{$NAME}}

One Paragraph of project description goes here

1. [💼 Getting Started](#markdown-header-getting-started)
  1.1 [📚 Prerequisites](#markdown-header-prerequisites)
  1.2 [📲 Installing](#markdown-header-installing)
2. [🛠 Testing](#markdown-header-testing)
  2.1 [🔁 E2E](#markdown-header-e2e)
  2.2 [🛁 Static Analysis](#markdown-header-static-analysis)
3. [🛥 Deployment](#markdown-header-deployment)
4. [🧾 Important Dependencies](#markdown-header-important-dependencies)
5. [🙌🏼 Contributing](#markdown-header-contributing)
6. [🏷 Versioning](#markdown-header-versioning)
7. [📜 Authors](#markdown-header-authors)
8. [📄 License](#markdown-header-license)
9. [📯 Acknowledgments](#markdown-header-acknowledgments)

## 💼 Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### 📚 Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

### 📲 Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## 🛠 Testing

Explain how to run the automated tests for this system

### 🔁 E2E

Explain what these tests test and why

```
npm run e2e
```

### 🛁 Static Analysis

Explain what these tests test and why

```
npm run test
```

## 🛥 Deployment

Add additional notes about how to deploy this on a live system

## 🧾 Important Dependencies

* [React](https://reactjs.org/) - The web framework used
* [NPM](https://www.npmjs.com/) - Dependency Management
* [DraftJS](https://draftjs.org/) - Framework for the Rich Text Editor

[logo]: /.bitbucket/logo.png "@inspire11/{{$NAME}}"
