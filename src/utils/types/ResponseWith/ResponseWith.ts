import { ResponseBody } from './../ResponseBody';

export interface ResponseWith<T> extends ResponseBody<T> {
}
