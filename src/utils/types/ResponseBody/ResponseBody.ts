export interface ResponseBody<T> extends Body {
  json(): Promise<T>;
}
