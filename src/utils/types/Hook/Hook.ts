export type Hook<Input = {}, Output = {}> = <T>(input?: Input) => Output;
