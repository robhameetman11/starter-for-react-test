import { ResponseWith } from '../ResponseWith';

export interface DataHookResponse<T> {
  readonly loading: boolean;
  readonly data: ResponseWith<T> | null;
  readonly error: Error | null;
}

export type DataHook<Input = {}> = <T>(input: Input) => DataHookResponse<T>;
