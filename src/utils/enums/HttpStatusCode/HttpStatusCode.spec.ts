import { isHttpStatusCode } from './HttpStatusCode';

describe('isHttpStatusCode', (): void => {
  describe('given 90', (): void => {
    it('should return false', (): void => {
      expect(isHttpStatusCode(90)).toBe(false);
    });
  });

  describe('given 100', (): void => {
    it('should return true', (): void => {
      expect(isHttpStatusCode(100)).toBe(true);
    });
  });

  describe('given 404', (): void => {
    it('should return true', (): void => {
      expect(isHttpStatusCode(404)).toBe(true);
    });
  });

  describe('given 403', (): void => {
    it('should return true', (): void => {
      expect(isHttpStatusCode(403)).toBe(true);
    });
  });

  describe('given ""', (): void => {
    it('should return false', (): void => {
      expect(isHttpStatusCode('')).toBe(false);
    });
  });

  describe('given null', (): void => {
    it('should return false', (): void => {
      expect(isHttpStatusCode(null)).toBe(false);
    });
  });

  describe('given undefined', (): void => {
    it('should return false', (): void => {
      expect(isHttpStatusCode(undefined)).toBe(false);
    });
  });
});
