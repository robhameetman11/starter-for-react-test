import { isHttpRequestMethod } from './HttpRequestMethods';

describe('isHttpRequestMethod', (): void => {
  describe('given "GET"', (): void => {
    it('should return true', (): void => {
      expect(isHttpRequestMethod('GET')).toBe(true);
    });
  });

  describe('given "POST"', (): void => {
    it('should return true', (): void => {
      expect(isHttpRequestMethod('POST')).toBe(true);
    });
  });

  describe('given "PUT"', (): void => {
    it('should return true', (): void => {
      expect(isHttpRequestMethod('PUT')).toBe(true);
    });
  });

  describe('given "PATCH"', (): void => {
    it('should return true', (): void => {
      expect(isHttpRequestMethod('PATCH')).toBe(true);
    });
  });

  describe('given "DELETE"', (): void => {
    it('should return true', (): void => {
      expect(isHttpRequestMethod('DELETE')).toBe(true);
    });
  });

  describe('given "b76rh4ge6"', (): void => {
    it('should return false', (): void => {
      expect(isHttpRequestMethod('b76rh4ge6')).toBe(false);
    });
  });
});
