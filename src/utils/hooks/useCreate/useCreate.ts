import { DataHook, DataHookResponse } from './../../../utils';
import { HttpRequestMethods } from './../../enums';
import { useFetch } from './../useFetch';

export interface UseCreateInput {
  readonly url: string;
  readonly headers?: Headers;
  readonly body: FormData | null;
}

export type UseCreateResponse<T> = DataHookResponse<T>;

export const useCreate: DataHook<UseCreateInput> = <T>({ url, body = null, headers = {} }) => {
  return useFetch<T>({
    request: new Request(url, {
      method: HttpRequestMethods.POST,
      headers,
      body,
    }),
  });
};
