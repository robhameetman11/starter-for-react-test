import { DataHook, DataHookResponse } from './../../../utils';
import { HttpRequestMethods } from './../../enums';
import { useFetch } from './../useFetch';

export interface UseDeleteInput {
  readonly url: string;
  readonly headers?: Headers;
}

export type UseDeleteResponse<T> = DataHookResponse<T>;

export const useDelete: DataHook<UseDeleteInput> = <T>({ url, headers = {} }) => {
  return useFetch<T>({
    request: new Request(url, {
      method: HttpRequestMethods.DELETE,
      headers,
    }),
  });
};
