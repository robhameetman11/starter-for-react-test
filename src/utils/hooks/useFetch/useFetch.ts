import { useState, useEffect } from 'react';
import { DataHook, DataHookResponse } from './../../types';

export interface UseFetchInput {
  readonly request: RequestInfo;
  readonly deferred?: boolean;
}

export type UseFetchResponse<T> = DataHookResponse<T>;

export const useFetch: DataHook<UseFetchInput> = <T>({ request, deferred = false }) => {
  const [loading, setLoading] = useState(!deferred);
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);

  useEffect(async (): Promise<void> => {
    setLoading(true);

    try {
      const res = await fetch(request);
      const data: T | null = await res.json();

      setData(data ? data : null);
    } catch (err) {
      setError(err);
    } finally {
      setLoading(false);
    }
  });

  return {
    data,
    error,
    loading,
  };
};
