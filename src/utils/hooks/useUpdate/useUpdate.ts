import { DataHook, DataHookResponse } from './../../../utils';
import { HttpRequestMethods } from './../../enums';
import { useFetch } from './../useFetch';

type Method = HttpRequestMethods.PUT | HttpRequestMethods.PATCH;

export interface UseUpdateInput {
  readonly url: string;
  readonly headers?: Headers;
  readonly body: FormData | null;
  readonly method: Method;
}

export type UseUpdateResponse<T> = DataHookResponse<T>;

export const useUpdate: DataHook<UseUpdateInput> = <T>({ url, method = HttpRequestMethods.PUT, body = null, headers = {} }) => {
  return useFetch<T>({
    request: new Request(url, {
      method,
      headers,
      body,
    }),
  });
};
