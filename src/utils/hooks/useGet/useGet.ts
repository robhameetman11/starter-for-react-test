import { DataHook, DataHookResponse } from './../../../utils';
import { HttpRequestMethods } from './../../enums';
import { useFetch } from './../useFetch';

export interface UseGetInput {
  readonly url: string;
  readonly headers?: Headers;
}

export type UseGetResponse<T> = DataHookResponse<T>;

export const useGet: DataHook<UseGetInput> = <T>({ url, headers = {} }) => {
  return useFetch<T>({
    request: new Request(url, {
      method: HttpRequestMethods.GET,
      headers,
    }),
  });
};
