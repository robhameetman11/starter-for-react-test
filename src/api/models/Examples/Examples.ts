import { Example, isExample } from './../Example';

export type Examples = Array<Example>;

export const isExamples = (value: unknown): value is Examples => {
  return Array.isArray(value)
    && value.every(innerVal => isExample(innerVal));
}
