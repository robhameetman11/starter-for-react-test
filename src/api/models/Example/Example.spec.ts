describe('isExample', (): void => {
  describe('given a valid Example', (): void => {
    it('should return true', (): void => {
      expect(isExample(validExample)).toBe(true);
    });
  });

  describe('given an invalid Example', (): void => {
    it('should return false', (): void => {
      expect(isExample(invalidExample)).toBe(false);
    });
  });
});
