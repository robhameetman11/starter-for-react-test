export interface Example {
  with: string | number;
}

export const isExample = (value: unknown): value is Example => {
  return typeof value === 'object'
    && value !== null
    && 'with' in value
    && (
      typeof (value as Partial<Example>).with === 'string'
      || typeof (value as Partial<Example>).with === 'number'
    )
    && Boolean((value as Partial<Example>).with);
};
