import { Examples, isExamples } from '../../../models';
import { DataHook, DataHookResponse, useEnv, useGet } from '../../../../utils';

export interface UseExamplesInput {
  readonly deferred: boolean;
  readonly request: RequestInfo;
}

export type UseExamplesResponse<T> = DataHookResponse<T>;

export const useExamples: DataHook<UseExamplesInput> = <E extends Examples = Examples>() => {
  const { API_ENDPOINT } = useEnv();

  return useGet<E>({
    url: `${API_ENDPOINT}/example`,
    validate: isExamples,
  });
};
