import { Example } from './../../../../api';
import { DataHook, DataHookResponse, useEnv, useGet } from './../../../../utils';

export interface UseExampleInput {
  readonly id: number;
}

export type UseExampleResponse<T> = DataHookResponse<T>;

export const useExample: DataHook<UseExampleInput> = <E extends Example = Example>({ id }) => {
  const { API_ENDPOINT } = useEnv();

  return useGet<E>({
    url: `${API_ENDPOINT}/example/${id}`,
  });
};
